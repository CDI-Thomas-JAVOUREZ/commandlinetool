package com.tjz;

import java.util.Map;

public class GetVars extends Command {

	String prop = "";
	String env = "";
	
	public GetVars(String[] arguments) {
		super("getvars", arguments);
		
		for(String s : arguments) {
			switch (s) {
			case "-env": env = getEnv();
			case "-prop": prop = getProp();
			}
		}
		
		if(prop.equals("") && env.equals("")) {
			env = getEnv();
			prop = getProp();
		}
				
				
	}

	@Override
	public String process() {
		// TODO Auto-generated method stub
		return env + prop;
	}
	
	private String getEnv() {
		
		StringBuilder res = new StringBuilder();
		
		for(Map.Entry<String, String> v : System.getenv().entrySet()) {
			res.append(v.getKey() + " : "  + v.getValue() + "\n");
		}
		
		return res.toString()+"\n";
	}
	
	private String getProp() {
		
		StringBuilder res = new StringBuilder();
		
		System.getProperties().entrySet();
		
		return System.getProperties().toString();
	}

}
