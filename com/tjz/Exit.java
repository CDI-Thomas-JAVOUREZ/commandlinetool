package com.tjz;

public class Exit extends Command {
	
	public Exit() {
		super("exit",null);
	}

	@Override
	public String process() {

		System.out.println("Sortie");
		System.exit(0);
		return null;
	}

}
