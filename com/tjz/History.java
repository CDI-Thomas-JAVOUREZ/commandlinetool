package com.tjz;

public class History extends Command {

	public History(String[] arguments) {
		super("history", arguments);
	}

	@Override
	public String process() {
		StringBuilder res = new StringBuilder();
		
		for(String s : arguments) {
			if(s != null)
				res.append(s).append("\n");
		}
		// TODO Auto-generated method stub
		return res.toString();
	}

}
