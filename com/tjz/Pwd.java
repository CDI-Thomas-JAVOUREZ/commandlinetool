package com.tjz;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Pwd extends Command {
	
	Path path;
	public Pwd(String path) {
		super("pwd", null);
		
		this.path = Paths.get(path);
	}

	@Override
	public String process() {
		// TODO Auto-generated method stub
		return this.path.toAbsolutePath().toString() + "\n";
	}
	
}
