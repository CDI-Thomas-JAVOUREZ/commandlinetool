package com.tjz;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Copy extends Command {

	File origin;
	
	File destination;
	
	public Copy(Path path, String[] arguments) 
			throws Exception {
		super("copy", arguments);
		
		try {
		if(arguments.length != 3) {
			throw new Exception("Nombre d'arguments invalide"); 
		}
		
		origin = Paths.get(path.toString(), arguments[1]).toFile();
		
		if(!origin.exists()) {
			throw new Exception("Le fichier d'origine n'existe pas.");
		}
		
		if(origin.isDirectory()) {
			throw new Exception("L'origine est un repertoires");
		}
		
		if(!origin.canWrite()) {
			throw new Exception("Copie interdite pour ce fichier");
		}
		
		destination = Paths.get(arguments[2]).toFile();
		
		if(destination.exists()) {
			throw new Exception("Le fichier d'origine n'existe pas.");
		}
		
		if(!destination.canWrite()) {
			throw new Exception("Ecriture impossible dans ce repertoire");
		}
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
				
	}

	@Override
	public String process() {
		
		try {
			Files.copy(origin.toPath(), destination.toPath());
			return "Le fichier " + origin + " a ete copie vers " + destination;
		}
		catch(Exception e) {
			
			return e.getMessage();
			
		}

	}

}

