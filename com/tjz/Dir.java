package com.tjz;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Dir extends Command {

	private Path directory;

	public Dir(String s) {
		super("dir", null);
		directory =Paths.get(s);
	}

	@Override
	public String process() {

		StringBuilder res = new StringBuilder();

		//Pour lister un r�pertoire, il faut utiliser l'objet DirectoryStream
		//L'objet Files permet de cr�er ce type d'objet afin de pouvoir l'utiliser
		try(DirectoryStream<Path> listing = Files.newDirectoryStream(directory)){

			int nbFiles = 0;
			int nbFolders = 0;

			for(Path nom : listing){
				if(Files.isDirectory(nom)) {
					nbFolders++;
					res.append("<DIR> " + nom.toFile().getName() + "\n");
				}
				else {
					nbFiles++;
					res.append("      " + nom.toFile().getName()+ "\n");
				}

			}

			res.append(nbFiles + " Fichiers" + "\n");
			res.append(nbFolders + " Dossiers"+ "\n");

		} catch (IOException e) {
			e.printStackTrace();
		}

	return res.toString();
}

}
