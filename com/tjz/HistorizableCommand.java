package com.tjz;

import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class HistorizableCommand extends Command implements Historizable {
	
	private static final SimpleDateFormat FORMATER = new SimpleDateFormat("hh:mm dd/MM/yyyy");

	public HistorizableCommand(String name, String[] arguments) {
		super(name, arguments);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String historize() {

		return FORMATER.format(new Date()) + " - " + this.toString();
	}

}
