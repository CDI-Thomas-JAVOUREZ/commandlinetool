package com.tjz;

public class ArgumentNotValidException extends Exception {

	public ArgumentNotValidException() {
		// TODO Auto-generated constructor stub
	}

	public ArgumentNotValidException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ArgumentNotValidException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ArgumentNotValidException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ArgumentNotValidException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
