package com.tjz;

public abstract class Command {
	
	protected String name; 
	protected String description;
	protected String[] arguments;
	
	public Command(String name, String[] arguments) {
		super();
		this.name = name;
		this.arguments = arguments;
	}
	
	public abstract String process();
	
	protected static final Integer parseInt(String s) {
		
		try {
			return Integer.parseInt(s);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	protected static final Long parseLong(String s) {
		
		try {
			return Long.parseLong(s);
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	public String toString() { 
		StringBuilder res = new StringBuilder();
		
		res.append(name);
		
		if(arguments != null) {
			for(String s: arguments) {
				res.append(" ").append(s);
			}
		}
		
		return res.toString();
	}
	
	public String getName() {return name;}
	
	
	public String[] getArguments() {return arguments;}


	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}


	public void setName(String name) {
		this.name = name;
	}
	
}
