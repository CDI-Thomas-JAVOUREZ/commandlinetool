package com.tjz;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Find extends Command {

	Path directory;
	
	String startsWith = null;
	String endsWith = null;
	
	String filename = null;
	
	public Find(Path path, String[] arguments)
			throws Exception {
		super("find", arguments);
		
		this.directory = path;
		
		if(arguments.length !=2 && arguments.length != 3 && arguments.length != 5)
			throw new Exception("Nombre d'arguments incorrects");
		
		if(arguments.length == 2) {
			this.filename = arguments[1];
		}
		
		if(arguments.length >2)
			checkArguments(arguments[1], arguments[2]);
		
		if(arguments.length == 5) {
			checkArguments(arguments[3], arguments[4]);
		}
		
	
	}

	@Override
	public String process() {
		StringBuilder res = new StringBuilder();
		final AtomicInteger nbFiles = new AtomicInteger();
		try (Stream<Path> search = Files.find(directory, 100, (x,y)->x.toFile().canRead()&&!y.isDirectory() && Pattern.matches(getExpr(),x.toFile().getName()))) {
			
		
		search.forEach(x->{res.append( x + "\n");
							nbFiles.getAndIncrement();
						});
		res.append(nbFiles+ " Fichiers trouv�s");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return res.toString();
	}
	
	private void checkArguments(String option, String value)
		throws Exception { 
		switch(option) {
		case "-starts": this.startsWith = value;break;
		case "-ends" : this.endsWith = value; break;
		default : throw new Exception("Option non reconnue");
		}
	}
	
	private String getExpr() {
		
		if(filename != null) {
			return filename;
		}
		
		StringBuilder res = new StringBuilder();
		
		if(startsWith != null) {
			res.append("^" + startsWith);
		}
		
		res.append(".*");
		
		if(endsWith != null) {
			res.append(endsWith + "$");
		}
		
		return (res.toString());
	}

}
