package com.tjz;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CommandBuilder {
	
	private String[] commandsHistory;
	
	private static int historySize;
	
	private Path path;
	
	CommandBuilder() {
		commandsHistory = new String[10];
		historySize = 0;
		
		this.path = Paths.get("./").toAbsolutePath();
	}
	
	
	public Command createCommand(String[] arguments)  {
		
		try {
			
			Command c = null;
		
			switch(arguments[0].toLowerCase()) {
			case "exit" : c = new Exit(); break;
			
			case "quit" : c = new Exit(); break;
			
			case "help" : c = new Help();break;
			
			case "pwd" : c = new Pwd(path.toString());break;
			
			case "river" : c = new River(arguments);break;
			
			case "isprime" : c = new IsPrime(arguments);break;
			
			case "history" : c = new History(commandsHistory);break;
			
			case "histclear" : {
				for(String s : commandsHistory) {
					s = null;
				}
				
				historySize = 0;
				
			}
			
			case "dir": c = new Dir(path.toString());break;
			
			case "cd" : this.cd(arguments[1]); break;
			
			case "find" : c = new Find(path, arguments); break;
			
			case "cat" : c = new Cat(path, arguments); break;
			
			case "copy" : c = new Copy(path, arguments); break;
			
			case "crf" : c = new CRF(path, arguments); break;
			
			case "crd" : c = new CRD(path, arguments); break;
			
			case "getvars" : c = new GetVars(arguments); break;
			
			default: c = null;
			
			}
			
			
			if(c instanceof HistorizableCommand) {
				this.putInHistory((HistorizableCommand) c);

			}
			
			return c;
			
		} catch (Exception e) {
			return null;
		}
		
		

	}
	
	private void putInHistory(HistorizableCommand c) {
		if(historySize < 10) {
			commandsHistory[historySize] = c.historize();
			historySize++;
		}
		else {
			for(int i = 0 ; i < commandsHistory.length - 1 ; i++) {
				commandsHistory[i] = commandsHistory[i+1];
			}
			
			commandsHistory[commandsHistory.length-1] = c.historize();
		}
	}
	
	private void cd(String s) {
		
		try {
			Path p = Paths.get(this.path.toString(), s);
	
			if(Files.isReadable(p)) {
				this.path = p.normalize();
			}
			
		}
		catch (Exception e){
			
		}
			
	
	}
	


}
