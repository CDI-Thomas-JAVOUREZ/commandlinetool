package com.tjz;

public class River extends Command {

	long r1;
	long r2;
	
	public River(String[] arguments) 
			throws Exception {
		super("river", arguments);
		
		try {
		if(arguments == null || arguments.length < 3) {
			throw new Exception("Nombre incorrect d'arguments");
		}
		
		r1 = Command.parseLong(arguments[1]);
		r2 = Command.parseLong(arguments[2]);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
	}

	@Override
	public String process() {
		// TODO Auto-generated method stub
		return riverResult(r1,r2)+"";
	}
	
	private static long riverResult(long r1,long r2) {
        
        while(r1 != r2) {
            if(r1>r2){
                r2 = RiverNext(r2);
            }
            else{
                r1 = RiverNext(r1);
            }
        }


       return r1;
    }
    
    
    public static long RiverNext(long r) {
    
    long result = r;
    
    do {
        result += r%10;
        r = r/10;
    }while(r/10 > 0);
    
    result += r%10;
    
    return result;
    
    }
    

}
