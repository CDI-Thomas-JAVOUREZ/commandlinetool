package com.tjz;

public interface Historizable {
	
	public String historize();

}
