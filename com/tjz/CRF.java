package com.tjz;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CRF extends Command {
	
	File f;

	public CRF(Path path, String[] arguments)
				throws Exception {
		
		super("crf", arguments);
		
		if(!path.toFile().canWrite()) {
			throw new Exception("Vous n'avez pas les droits pour cette action");
		}
		
		f= Paths.get(path.toString(),arguments[1]).toFile(); 
		
		if(f.exists()) {
			throw new Exception("Le fichier existe deja");
		}
			
	}

	@Override
	public String process() {
		try {
		f.createNewFile();
		} catch (Exception e) {
			
		}
		
		// TODO Auto-generated method stub
		return "Le fichier " + f.getName() + " a ete cree.";
	}

}
