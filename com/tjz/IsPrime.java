package com.tjz;

public class IsPrime extends Command {

	private long number;
	
	public IsPrime(String[] arguments) {
		super("isprime", arguments);
		// TODO Auto-generated constructor stub
		try {
			number = Command.parseLong(arguments[0]);
		}
		catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
	}

	@Override
	public String process() {
		
		return number + ( prime() ? " est " : " n'est pas ") + "un nombre premier";
	}
	
	private boolean prime() {
		if (number < 2) {
			return false;
		}
		
		for(int i = 2 ;i < number;i++) {
			if(number%i == 0 ) {
				return false;
			}
		}
		
		return true;
	}

}
