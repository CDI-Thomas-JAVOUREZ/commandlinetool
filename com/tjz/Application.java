package com.tjz;

import java.util.Scanner;

public class Application {
	
	private static CommandBuilder commandBuilder;

	public static void main(String[] args) {
		
		commandBuilder = new CommandBuilder();
		
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.print("> ");
					
			try {
				
				Command c = commandBuilder.createCommand(sc.nextLine().split(" "));
				
				if(c != null) {
					System.out.println(c.process());
				}
				
			}
			catch(Exception e) {
				System.out.println(" La commande n'a pu aboutir : " + e.getMessage());
				continue;
			}

		}
			


	}

}
