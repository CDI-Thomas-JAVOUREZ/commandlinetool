package com.tjz;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CRD extends Command {

	File f;
	
	public CRD(Path path, String[] arguments) {
		
		super("crd", arguments);
		
		try {
		
		if(!path.toFile().canWrite()) {
			throw new Exception("Vous n'avez pas les droits pour cette action");
		}
		
		f= Paths.get(path.toString(),arguments[1]).toFile(); 
		
		if(f.exists()) {
			throw new Exception("Le fichier existe deja");
		}
		
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	@Override
	public String process() {
		try {
		f.mkdir();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		
		return "Le repertoire " + f.getName() + " a ete cree.";
	}

}
