package com.tjz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Cat extends Command {
	
	File f;

	public Cat(Path path, String[] arguments) 
			throws Exception {
		
		super("cat", arguments);
		
		if(arguments.length != 2) {
			throw new Exception("Nombre d'arguments incorrects");
		}
		
		f = Paths.get(path.toString(),arguments[1]).toFile();
		
		if(!f.exists()) {
			throw new Exception("Le fichier n'existe pas");
		}
		
		if(f.isDirectory()) {
			throw new Exception("Il s'agit d'un repertoire");
		}
		
		if(!f.canRead()) {
			throw new Exception("Vous ne disposez pas des droits de lecture sur ce fichier");
		}
	
	}

	@Override
	public String process() {
		
		StringBuilder res = new StringBuilder();
		
		System.err.println(f.toString());
		
		try{
			InputStream flux=new FileInputStream(f); 
			InputStreamReader lecture=new InputStreamReader(flux);
			BufferedReader buff=new BufferedReader(lecture);
			
			String ligne;
			while ((ligne=buff.readLine())!=null){
				res.append(ligne).append("\n");
			}
			buff.close(); 
			}		
			catch (Exception e){
			System.out.println(e.toString());
			}
		
		
		// TODO Auto-generated method stub
		return res.toString();
	}

}
